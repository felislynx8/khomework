psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER user;
    ALTER USER test WITH PASSWORD 'user!123I';
    CREATE DATABASE test owner test;
    GRANT ALL PRIVILAGES ON DATABASE test TO test;
EOSQL