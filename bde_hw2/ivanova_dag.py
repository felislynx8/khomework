from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.models import Variable
from airflow.sensors.python import PythonSensor
from airflow.exceptions import AirflowSensorTimeout
from datetime import datetime
import requests as r
from airflow.operators.bash_operator import BashOperator
default_args = {
                'start_date': datetime(2023, 1, 1)
                }

def S3KeySensor():
  import requests as r
  from xml.etree import ElementTree

  resp = r.get("https://storage.yandexcloud.net/misis-zo-bigdata?list-type=2&encoding-type=url")
  tree = ElementTree.fromstring(resp.text)

  if len(tree)>=7:
    return True
  else:
    return False


def S3Json2CSV():
  import urllib
  import json
  import csv
  from xml.etree import ElementTree
  import datetime
  def fix_string(someline):
        if isinstance(someline, str):
            newline = someline.replace('\n','\\n').replace(';' , '_')
            return newline
        else:
            return someline

  def getRows(data,columns):
        day_now = str(datetime.date.today())
        output = []
        output.append(list(columns))
        output[0].append('date')
        for i, line in enumerate(data):
            row = json.loads(line.decode('utf-8'))
            tmp = [fix_string(row[key]) if key in row.keys() else ''for key in columns]
            tmp.append(day_now)
            output.append(tmp)
        return output

  #Parse filename
  key = ElementTree.fromstring(r.get("https://storage.yandexcloud.net/misis-zo-bigdata?list-type=2&encoding-type=url").text)[6][0].text

  base_url = "https://storage.yandexcloud.net/misis-zo-bigdata/"
  data = urllib.request.urlopen(base_url+key)
  columns = ['overall', 'verified', 'reviewTime', 'reviewerID', 'asin', 'reviewerName', 'reviewText', 'summary', 'unixReviewTime']

  fname = "/tmp/new_data.csv"
  with open(fname,'w') as outf:
      outcsv = csv.writer(outf)
      outcsv.writerows(getRows(data,columns))




def _failure_callback(context):
  if isinstance(context['exception'], AirflowSensorTimeout):
    print(context)
  print("Sensor timed out")

with DAG('ivanova_dag', schedule_interval='@daily', default_args=default_args, catchup=False) as dag:
  s3_check = PythonSensor(
  task_id='s3keysensor',
  poke_interval=120,
  timeout=30,
  mode="reschedule",
  python_callable=S3KeySensor,
  on_failure_callback=_failure_callback,
  soft_fail=True
  )

# converting json to csv
  json2csv = PythonOperator(
  task_id="json2csv",
  python_callable=S3Json2CSV,
  trigger_rule='none_failed_or_skipped'
  )
  bash_task = BashOperator(task_id='bash_task', bash_command="hdfs dfs -put -f /tmp/new_data.csv /user/ivanova/airflow/new_data.csv")
  hql_create_db = """
        CREATE DATABASE IF NOT EXISTS mydb_ivanova;
    """

  hql0 = """CREATE TABLE IF NOT EXISTS tmp_all_data1 (
    overall string,
    verified string,
    reviewtime string,
    reviewerid string,
    asin string,
    reviewername string,
    reviwertext string,
    summary string,
    unixreviewtime int
  );
LOAD DATA INPATH '/user/ivanova/airflow/new_data.csv'
OVERWRITE INTO TABLE tmp_all_data1;
 """
  hql2 = """
  CREATE TABLE IF NOT EXISTS all_ratings(
            overall string,
            verified string,
            reviewtime string,
            reviewerid string,
            asin string,
            reviewername string,
            reviewtext string,
            summary string,
            unixreviewtime int
          );
  INSERT INTO TABLE all_ratings
  SELECT * FROM tmp_all_data1;
"""

  hql3 = """
  CREATE TABLE IF NOT EXISTS user_scores(
            reviewerid string,
            asin string,
            summary string
          );
  INSERT INTO TABLE user_scores
SELECT reviewerid, asin, summary FROM tmp_all_data1;
"""

  hql4 = """
  CREATE TABLE IF NOT EXISTS reviews(
            overall string,
            reviewerid string,
            reviwertext string
          );
  INSERT INTO TABLE reviews
  SELECT reviewerid, reviwertext, overall FROM tmp_all_data1;
"""

  hql5 = """
  CREATE TABLE IF NOT EXISTS product_scores(
            asin string,
            summary string
          );
  INSERT INTO TABLE product_scores
  SELECT asin, summary FROM tmp_all_data1;
"""
  step_hive_db = HiveOperator(
    hql=hql_create_db,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_hive_use_db',
    run_as_owner=True,
    dag=dag)

  step_hive_table1 = HiveOperator(
    hql=hql0,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_hive_create_tmp_table',
    run_as_owner=True,
    dag=dag)

  step_to_parquet = HiveOperator(
    hql=hql2,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_insert_from_temp_to_rating',
    run_as_owner=True,
    dag=dag)

  step_to_parquet1 = HiveOperator(
    hql=hql3,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_insert_from_temp_to_userscores',
    run_as_owner=True,
    dag=dag)

  step_to_parquet2 = HiveOperator(
    hql=hql4,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_insert_from_temp_to_rewiews',
    run_as_owner=True,
    dag=dag)

  step_to_parquet3 = HiveOperator(
    hql=hql5,
    hive_cli_conn_id='hive_staging',
    schema='mydb_ivanova',
    hiveconf_jinja_translate=True,
    task_id='step_insert_from_temp_to_productscores',
    run_as_owner=True,
    dag=dag)



s3_check >> json2csv >> bash_task >> step_hive_db >>step_hive_table1

step_hive_table1 >> step_to_parquet
step_hive_table1 >> step_to_parquet1
step_hive_table1 >> step_to_parquet2
step_hive_table1 >> step_to_parquet3
if __name__ == '__main__':
    dag.cli()